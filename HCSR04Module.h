#ifndef HCSR04MODULE_H
#define HCSR04MODULE_H

#include <Arduino.h>

class HCSR04Module {
	private:
		const static byte BIT_DELAY = 2;
		byte trigPin;
		byte echoPin;
	public:
		HCSR04Module(byte, byte);
		void initial();
		float getDistance();
};

#endif