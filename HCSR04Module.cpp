#include "HCSR04Module.h"

/**
 * Constructs the HCSR04Module object passes the trigPin, echoPin.
 * @param byte trigPin The pin will be written from MCU.
 * @param byte echoPin The pin will be read from MCU.
 */
HCSR04Module::HCSR04Module(byte trigPin, byte echoPin) {
	this->trigPin = trigPin;
	this->echoPin = echoPin;
}

/*
Initials the configurations.
 */
void HCSR04Module::initial() {
	pinMode(this->trigPin, OUTPUT);
	digitalWrite(this->trigPin, HIGH);
	pinMode(this->echoPin, INPUT);
}

/**
 * Returns The distance between the target object and sensor.
 * @return float The distance between the target object and sensor.
 */
float HCSR04Module::getDistance() {
	digitalWrite(this->trigPin, LOW);
	delayMicroseconds(HCSR04Module::BIT_DELAY);
	float distance = 0.170145 * pulseIn(this->echoPin, HIGH);
	delayMicroseconds(HCSR04Module::BIT_DELAY);
	digitalWrite(this->trigPin, HIGH);
	delayMicroseconds(HCSR04Module::BIT_DELAY);
	return distance;
}