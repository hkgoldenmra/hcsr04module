#include "HCSR04Module.h"

byte TRIG_PIN = 2;
byte ECHO_PIN = 3;
HCSR04Module hcsr04Module = HCSR04Module(TRIG_PIN, ECHO_PIN);

void setup() {
	Serial.begin(115200);
	hcsr04Module.initial();
}

void loop() {
	Serial.print(hcsr04Module.getDistance());
	Serial.print("mm");
	Serial.println();
	delay(1000);
}